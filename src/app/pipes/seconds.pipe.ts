import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: 'appSecondsPipe'
})

export class SecondsPipe implements PipeTransform {
  transform(time: number): number {
    return time / 10;
  }
}
