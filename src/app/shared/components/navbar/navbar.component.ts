import { Location } from "@angular/common";
import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss']
})

export class NavbarComponent {
  @Input() isHaveNext: boolean;

  constructor(private location: Location) { }

  public goBack(): void {
    this.location.back();
  }
}
