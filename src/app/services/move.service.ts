import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class MoveService {
  private readonly BASE_URL = 'http://192.168.4.65:3000/';

  constructor(private http: HttpClient) { }

  public moveLeftServo(degree: number): Observable<any> {
    return this.http.get(this.BASE_URL.concat('left-servo'), { params: { position: degree.toString() }});
  }

  public moveRightServo(degree: number): Observable<any> {
    return this.http.get(this.BASE_URL.concat('right-servo'), { params: { position: degree.toString() }});
  }

  public moveBaseServo(degree: number): Observable<any> {
    return this.http.get(this.BASE_URL.concat('base-servo'), { params: { position: degree.toString() }});
  }
}
