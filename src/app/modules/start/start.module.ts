import { NavbarModule } from './../../shared/components/navbar/navbar.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { StartComponent } from './start.component';

@NgModule({
  declarations: [
    StartComponent
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: StartComponent
      }
    ]),
    NavbarModule
  ]
})

export class StartModule { }
