import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-start-stop',
  templateUrl: './start-stop.component.html',
  styleUrls: ['./start-stop.component.scss']
})
export class StartStopComponent implements OnDestroy {

  @Input() public title: string;
  @Output() public stateChange = new EventEmitter<boolean>();

  public isOn: boolean;
  public time = 0;

  private timer: any;

  constructor() { }

  public toggle(): void {
    this.isOn = !this.isOn;
    this.stateChange.next(this.isOn);

    if (this.isOn) {
      this.startTimer();
    } else {
      clearInterval(this.timer);
    }
  }

  private startTimer(): void {
    clearInterval(this.timer);
    this.time = 0;
    this.timer = setInterval(() => this.time++, 100);
  }

  public ngOnDestroy(): void {
    clearInterval(this.timer);
  }

}
