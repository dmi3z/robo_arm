import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MAX_VALUE, MIN_VALUE, STEP_DEGREE } from '../../constants/servo.constants';

@Component({
  selector: 'app-incr-decr',
  templateUrl: './incr-decr.component.html',
  styleUrls: ['./incr-decr.component.scss']
})
export class IncrDecrComponent {

  @Input() public title: string;
  @Output() public changeValue = new EventEmitter<number>();

  public currentValue = 0;

  constructor() { }

  public increment(): void {
    if (this.currentValue < MAX_VALUE) {
      this.currentValue += STEP_DEGREE;
      this.changeValue.next(this.currentValue);
    }
  }

  public decrement(): void {
    if (this.currentValue > MIN_VALUE) {
      this.currentValue -= STEP_DEGREE;
      this.changeValue.next(this.currentValue);
    }
  }
}
