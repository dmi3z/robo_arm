import { Component } from '@angular/core';
import { MoveService } from 'src/app/services/move.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['settings.component.scss']
})

export class SettingsComponent {

  constructor(private moveService: MoveService) { }

  public moveLeft(degree: number): void {
    this.moveService.moveLeftServo(degree).pipe(take(1)).subscribe();
  }

  public moveRight(degree: number): void {
    this.moveService.moveRightServo(degree).pipe(take(1)).subscribe();
  }

  public moveBase(degree: number): void {
    this.moveService.moveBaseServo(degree).pipe(take(1)).subscribe();
  }
}
