import { NavbarModule } from './../../shared/components/navbar/navbar.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SettingsComponent } from "./settings.component";
import { IncrDecrComponent } from './components/incr-decr/incr-decr.component';
import { StartStopComponent } from './components/start-stop/start-stop.component';
import { SecondsPipe } from 'src/app/pipes/seconds.pipe';

@NgModule({
  declarations: [
    SettingsComponent,
    IncrDecrComponent,
    StartStopComponent,
    SecondsPipe
  ],
  imports: [
    CommonModule,
    NavbarModule,
    RouterModule.forChild([
      {
        path: '',
        component: SettingsComponent
      }
    ])
  ]
})

export class SettingsModule { }
